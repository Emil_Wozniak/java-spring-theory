# Table of content
- [Java](assets/Java.md)
- [SOLID](assets/SOLID.md)
- [Spring](assets/Spring.md)
- [Komunikacja sieciowa](assets/Comminication.md)
- [Data Structure](assets/data_structure.md)
- [Design Patterns](assets/design_pattern.md)
- [Transaction](assets/transaction.md)
- [Database](assets/Database.md)
