# Komunikacja sieciowa

[[_TOC_]]

## SOAP i REST
- oba to protokoły komunikacji serwisów internetowych.
- oba są skalowalne.
- integrują się z SSL.

### *SOAP* (Simple Object Access Protocol)
- był długo standardem komunikacji sieciowej.
- wykonuje operacje na podstawie zestawu wzorców przesyłania wiadomości.
- zaprojektowany przez **Microsoft**.
- zapewnia *wyłącznie* format **XML**.
- korzyści:
    - większe bezpieczeństwo (**WS-Security**)
    - build-in retry logic
    
### *REST* (Representational State Transfer)
- bazuje na pojedynczym, spójnym interface'ie dostępu do nazwanych zasobów.
- używany, kiedy dostarczamy publiczne API.
- dostarcza dostarcza dane.
- korzyści:
    - zapewnia większą różnorodność formatów (**Text**, **JSON**, **XML**, ... etc. ).
    - domyślnie korzysta z **JSON**.
    - ma większą wydajność dzięki cache'owaniu.
    - zużywa mniej przepustowości i jest szybszy.
    - łatwiejszy w integracji.

## Idempotencja
Idempotencja oznacza, że wykonanie jednej operacji lub jej wielokrotne zastosowanie daje ten sam efekt.

## Idempotencja w komunikacji REST-owej
- **POST**: NIE jest idempotentny.
- **GET**, **PUT**, **DELETE**, **HEAD**, **OPTIONS** i **TRACE**: są idempotentne. 

Metoda idempotentna oznacza, że wynik pomyślnie wykonanego żądania jest niezależny od liczby jego wykonań.

### POST
- służy do tworzenia nowego zasobu na serwerze.
- kiedy wywołasz to samo żądanie POST *N* razy, będziesz mieć *N* nowych zasobów na serwerze.

### GET, HEAD, OPTIONS i TRACE
- te metody NIGDY nie zmieniaj stanu zasobów na serwerze.

### PUT
- służy (niekoniecznie) do aktualizacji stanu zasobów.
- pierwsze żądanie zaktualizuje zasób, każde następne żądania po N-1 będą po prostu nadpisywać ten sam stan zasobów 
wielokrotnie.

### DELETE
- pierwsze żądanie spowoduje usunięcie zasobu, a odpowiedź będzie wynosić `200 (OK)` lub `204 (No Content)`.
- następne po N-1 zapytania zwrócą `404 (Not fount)`.
- nie ma zmiany stanu żadnego zasobu po stronie serwera, ponieważ oryginalny zasób został już usunięty.