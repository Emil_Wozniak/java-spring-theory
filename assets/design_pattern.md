# Wzorce projektowe

Wzorce opisane w tym repozytorium oparte są na stronie www.refactoring.guru 
i książce *Wzorce projektowe*.

## Spis treści

**Kreacyjne**
- [Wzorzec Budowniczy](patterns/Builder.md)

**Strukturalne:**
- [Wzorzec Adapter](patterns/Adapter.md)
- [Wzorzec Bridge](patterns/Bridge.md)
- [Wzorzec Composite](patterns/Composite.md)
- [Wzorzec Pełnomocnik (Proxy)](patterns/Proxy.md)