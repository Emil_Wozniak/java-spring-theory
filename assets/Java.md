# Pytania rekrutacyjne JAVA

[[_TOC_]]

## Co to znaczy, że Java jest językiem obiektowym?
- identyfikacja rzeczywistych przedmiotów za pomocą klas
- klasy posiadają poszczególne elementy i zachowania.

## Co to jest i do czego służy Hibernate?
- to Framework.
- służy do realizacji warstwy dostępu do Bazy danych.
- pod projekty:
    - *Core*: centralna część wszystkich projektów, pozwalana na mapowanie obiektów na Javę
    - *Annotations*: rozszerza *Core* pozwala na używanie adnotacji zamiast XML.
    - *EntityManager*: rozszerza *Core*, wprowadza klasę **EntityManager** (EM), z którego poziomu dobywa się komunikacja z Bazą danych. Z *Annotation* pozwalają na posługiwanie się techniką **JPA**
    - *Shards*: rozszerza *Core*, dla wielu baz danych.
    - *Search*: rozszerza *Core* o usługę full-text-search oparty o Lucene.
    - *Tools*: zestaw narzędzi do łatwiejszego wykorzystania *Core*.

## Czym w Javie różni się final od finally i finalize?
- *final*: modyfikator, który uniemożliwia:
    - zmianę wartości pola
    - przesłonięcie metody w dziedziczeniu
    - rozszerzenie klasy
- *finally*: 
    - element bloku try-catch
    - kod w nim zawarty zostanie wykonany niezależnie od wyniku **try**
    - zazwyczaj do zamknięcia otwartych zasobów i wyczyszczenia pamięci
- *finalize*:
    - metoda klasy *Object*
    - pozwala na wykonanie czynności, zanim GC usunie obiekt.
    - nie jest rekomendowane używanie z powodu możliwości niepowodzenia.

## Co to jest JIRa?
Narzędzie do śledzenia błędów i zarządzania projektem

## Czym jest Spring?
Framework upraszczający tworzenie projektu webowego

## Na czym polega technika TDD?
- skrót od Test-Driven Development
- technika tworzenia oprogramowania na podstawie testów.
- zgodnie z nią najpierw należy napisać testy a później implementację.

## Czym są wzorce projektowe?
- zbiór wskazówek do rozwiązania problemów programowania obiektowego.
- wzorce:
    - *Fabryka*: to metoda, która zwraca interface, zawiera logikę tworzącą właściwy obiekt. W Springu `@Bean`.
    - *Singeleton*: w Springu domyślnie `@Service`
    - *Fasada*: pozwala na wystawienie uproszczonego interface'u programistycznego poprzez wprowadzenie dodatkowej warstwy.

## Czym jest GIT?
To system kontroli wersji

## Jaka jest różnica pomiędzy equals() a „==”?
- *equals* 
    - jest metodą klasy **Object**.
    - powinna być nadpisana w każdym obiekcie dziedziczącym po klasie **Object**.
    - sprawdza wartość obiektów tego samego typu.
- *==*:
    - jest operatorem.
    - służy do sprawdzenia referencji obiektów w pamięci.

## Czym jest JVM?
- to maszyna wirtualna języka Java
- wykorzystuje kod bytowy Javy.
- kod bytowy Java można odpalić na dowolnym JVM bez względu na OS.

## Dziedziczenie w języku Java
- to mechanizm współdzielenia funkcjonalności między klasami.
- klasa dziedzicząca posiada atrybuty i zachowania klasy, po której dziedziczy.

## Garbage Collector
- jest programem.
- jego głównym zadaniem jest usuwanie z pamięci nieużywanych obiektów.

### Zasady działania
1. poszukiwanie 'żywych obiektów'.
2. usuwanie pozostałych obiektów z pamięci.
3. ewentualna defragmentacja pamięci.

## String, StringBuffer i StringBuilder
- **String**
    - tworzy niemodyfikowalne ciągi znaków.
    - po utworzeniu stringa kompilator najpierw sprawdza, czy istnieje on już w string pool, 
    jeśli tak zwraca referencję do niego, w innym wypadku tworzy nowy ciąg znaków w `SpringPool`, 
    a następnie zwraca referencję do tego obiektu.
- **StringBuffer**
    - jest synchroniczny.
    - działa na jednym wątku, przez co jest wolniejszy niż `StringBuilder`.
    - jest bezpieczny dla wątków.
    - od Java 1
- **StringBuilder**
    - jest asynchroniczny.
    - nie jest bezpieczny dla wątków.
    - od Java 5
    - jest szybszy.

## Typy numeryczne

### **Liczby całkowite**

| Typ   | Wielkość | Zakres                   | znaki |
| ------|--------- |---:                      |---:   |
| byte  | 1 byte   | -128 <-> 127             |   -   |
| short | 2 byte   | -32 768 <-> 32 767       |   -   |
| int   | 4 byte   | -2'147 mln <-> 2'147 mln |   -   |
| long  | 8 byte   | -2^63 <-> (2^63) -1      | l / L |

### **Liczby zmiennoprzecinkowe**

| Typ    | Wielkość | Zakres                   | znaki |
| -------|--------- |---:                      |---:   |
| float  | 4 byte   | 6-7 miejsce po przecinku | F / f |
| double | 8 byte   | 15 miejsce po przecinku  | D / d |

## Klasa abstrakcyjna a interfejs

### **Klasa abstrakcyjna**
- nie posiada konstruktora
- można po niej dziedziczyć za pomocą `extend`.
- może posiadać metody zawierające ciało wyrażeniowe.
- może zawierać metody oznaczone jako `abstract`, która musi zostać zaimplementowana w klasie dziedziczącej.
- metody możemy oznaczać jako `public` lub `protected`.
- może posiadać stan w polach, który może być modyfikowany.

### **Interface**
- jest typem.
- może przechowywać stałe wartości.
- nie może mieć stanu.
- służą do definiowania zachowań dla obiektów.
- Jeśli posiada tylko jedną metodę, można go oznaczyć jako interface funkcyjny (Java 8).
- może posiadać metody `default` (Java 8).
- może posiadać metody prywatne (Java 1.9).

### Klasa anonimowa
- Interface i Klasa abstrakcyjna mogą zostać implementowane jako obiekt, tworząc klasę i obiekt w tym samym miejscu.

## Enkapsulacja
- Hermetyzacja dawniej „kapsułkowanie”, od ang. encapsulation.
- polega na ukrywaniu danych składowych klasy tylko dla metod tej klasy.
- klasa posiadające wszystkie pola prywatne lub chronione jest klasą hermetycznie pełną.
- klasa hermetyczna powinna posiadać interface wyodrębniający metody, 
ten zbiór zapewnia jedyną komunikację między inną klasą a klasą hermetyczną.

## Co to jest polimorfizm?
- mechanizm pozwalający używać wartości, zmienne i podprogramy na kilka różnych sposobów.
- polega na zastosowaniu abstrakcji i implementacji konkretnego zastosowania.
- to system decyduje o szczegółach, a nie programista.

## Obiekty niemutowalne
- to takie, które nie pozwalają na zmianę przechowywanych wartości (stanu) w czasie istnienia tego obiektu.
- to jak zostały stworzone, zostanie z nim do końca.
- Jeżeli chcemy, aby jakaś wartość obiektu została zmieniona, będziemy musieli utworzyć nowy obiekt, któremu przekażesz aktualne dane.

## Kontrakt hashCode i equals
- mówi o tym, że jeżeli wartość hashCode dla 2 obiektów jest taka sama, to obiekty te mogą być równoznaczne. 
- Jeśli jednak hashCode jest różny, to equals zawsze zwróci false. 
- Na tym założeniu opiera się całe Collections API, w szczególności Set’y oraz wszelkie metody typu `contains()`,
`hasKey()` etc.

## Interfejs Comparable i Comparator

### Comparable
- dostarcza metodę `equalsTo`.
- Poza sprawdzaniem równości obiektów, tak jak w metodzie `equals`, oferuje jeszcze określanie, czy jest mniejszy 
lub większy niż inny obiekt. 
- pozwala nam na sortowanie obiektów w oczekiwanym porządku.

### Comparator
 - Możemy napisać swój własny lub wykorzystać już istniejące. Np. klasa `String` ma `Comparator`,
ignorujący wielkość liter.

## Wyjątki
- Wyjątek (ang. `exception`) jest specjalną klasą. Jest ona specyficzna, ponieważ w swoim łańcuchu dziedziczenia ma klasę
`java.lang.Throwable`. 

- Instancje, które w swojej hierarchii dziedziczenia mają tę klasę, mogą być „rzucone” (ang. `throw`) przerywając
standardowe wykonanie programu.

### Obsługa wyjątków
- Do obsługi wyjątków służy blok `try/catch`.
- Pod blokiem `try` może znajdować się wiele bloków `catch`.

### Obsługa kilku rodzajów wyjątków w jednym bloku catch
- Może zdarzyć się sytuacja, w której chciałbyś obsłużyć kilka wyjątków, a nie mają one wspólnej klasy bazowej. 
Wówczas w nawiasach po catch możesz oddzielić klasy wyjątków symbolem ` | ` jak w przykładzie poniżej.

### Rodzaje wyjątków checked oraz unchecked
- Każdy wyjątek w języku Java dziedziczy po klasie `Throwable`.

#### Checked Exceptions
- Muszą być obsłużone przez programistę, wymaga tego kompilator. 
- dziedziczy po `Exception`.

#### Unchecked Exceptions
- Przykładowym wyjątkiem typu unchecked jest IllegalArgumentException, natomiast `IOException` jest wyjątkiem typu 
checked.
- dziedziczy po `RuntimeException`.

### Klauzula throws
Wyjątek można obsłużyć na dwa sposoby. 
- Jeden już znasz, to otoczenie fragmentu kodu blokami `try/catch`.
- Drugi sprowadza się do „zepchnięcia” odpowiedzialności obsłużenia wyjątku o poziom niżej, do metody wywołującej.