# Aspect

[[_TOC_]]

## Aspect
Moduł, który posiada zestaw funkcji API zapewniające obsługę wymagań przekrojowych. Na przykład, moduł logowania 
będzie wołał aspekt AOP rejestrujący logi. Aplikacja może mieć dowolną liczbę aspektów w zależności od 
zapotrzebowania. Aspekty Spring AOP, są realizowane z wykorzystaniem regularnych klas 
(podejście bazujące na schemacie) lub klas z adnotacją `@Aspect` (styl `@AspectJ`).

## Różnice między wymaganiami (concern) i wymaganiami przekrojowymi (cross-cutting concern) w AOP
- **Concern** (wymaganie): jest to zachowanie, które chcemy mieć w module aplikacji. Wymaganie może być zdefiniowana 
jako funkcja, którą chcemy zaimplementować. Zagadnienie, którym jesteśmy zainteresowani definiujące nasze problemy.
- **Cross-cutting concern** (wymaganie przekrojowa): Jest to problem, który dotyczy całej aplikacji, i wpływa na całą 
aplikację. na przykład logowanie, bezpieczeństwo i transferu danych są wymagania, które są potrzebne w niemal każdym 
module aplikacji, stąd wymagania przekrojowe.

## Join point
- Stanowi to punkt w twojej aplikacji, gdzie można podłączyć aspekt AOP.
- Można również powiedzieć, że jest to rzeczywiste miejsce w wymaganiach, 
gdzie zostanie podjęta akcja przy użyciu Spring AOP .

## Advice
- Jest to rzeczywista akcja, którą należy podjąć przed / lub po wykonaniu metody. 
- Jest to rzeczywista część kodu, która jest wywoływana podczas wykonywania programu przez framework Spring AOP.

## Pointcut
- Jest to zestaw jednego lub więcej punktów łączeń, gdzie Advice powinna zostać wykonana. 
- Można określić Pointcut za pomocą wyrażeń lub wzorów.

## Introduction
Introduction pozwala dodawać nowe metody lub atrybuty do istniejących klas.

## Target object
- Obiekt będący wyznaczonym przez jeden lub więcej aspektów. 
- Obiekt ten musi być zawsze obiektem proxy. 
- Musi być także określany jako wyznaczony obiekt.

## Weaving
Weaving to proces łączenia aspektów z innymi typami z aplikacji lub obiektami w tworzony docelowy obiekt (Target object).

## Punkty, w których weaving może być stosowany.
Weaving można zrobić w czasie kompilacji, podczas ładowania lub przy starcie.

## Typy advice
Aspekty mogą pracować z pięcioma rodzajami Advice wymienionymi poniżej:

- **before**: Uruchom advice przed wykonaniem metody.
- **after**: Uruchom advice po wykonaniu metody nie zależnie od jej wyniku.
- **after-returning**: uruchom advice po wykonaniu metody tylko wtedy, gdy metoda zakończy się pomyślnie.
- **after-throwing**: Uruchom advice po wykonaniu metody tylko wtedy gdy zostanie rzucony wyjątek.
- **around**: Uruchom advice przed i po tym, jak metoda zostanie wywołana.

## Implementacja aspektów bazująca na schemacie XML
Aspekty są realizowane z wykorzystaniem tagów z bazowej konfiguracji XML.

## @AspectJ, Bazowa implementacja aspektów
`@AspectJ` odnosi się do stylu deklarowania opartego na standardowych klasach Javy z wykorzystaniem adnotacji Java 5.
