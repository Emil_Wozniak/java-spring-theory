# MVC 

[[_TOC_]]

## Spring MVC framework
- Spring Web MVC framework zapewnia architekturę `Model-View-Controller` i gotowe komponenty
- komponenty mogą być wykorzystane do opracowania elastycznych i luźno powiązanych aplikacji internetowych.
- Wzorzec MVC powoduje oddzielenie różnych aspektów stosowania (logiki wejścia, logiki biznesowej i logiki UI).
- zapewnia jednocześnie luźne powiązania między tymi elementami.

## DispatcherServlet
- Jest zaprojektowany na podstawie `DispatcherServlet`, 
- obsługuje wszystkie żądania i odpowiedzi `HTTP`.

## WebApplicationContext
- Jest rozszerzeniem zwykłego `ApplicationContext`. 
- ma kilka dodatkowych funkcji niezbędnych dla aplikacji internetowych. 
- Różni się on od normalnego `ApplicationContext` tym, że jest w stanie rozwiązać żądania, i za pomocą własnego `Servleta`.

## Spring MVC vs Struts MVC ?
Oto niektóre z zalet Spring MVC, które czynią go lepszym od Struts MVC:

| Spring MVC | Struts     |
|------------|------------|
| Spring MVC jest bardzo wszechstronny i elastyczny oparty na interfejsach | Struts narzuca Action i obiekt Form w formie dziedziczenia.
| Spring zapewnia zarówno przechwytywacze, jak i kontrolery, a tym samym pomaga czynnik wypracować wspólną zachowanie dla obsługi wielu żądań. |
| Spring może być skonfigurowane z różnymi [technologiami widoków](#Technologie Widoków) |
| W Spring MVC Kontrolery mogą być konfigurowane za pomocą `DI` (`IoC`), który sprawia, że jego testy i integracja są łatwe. |
| Spring nie zmusza kontrolerów do rozszerzania, może istnieć wiele implementacji kontrolerów, które można wybrać do rozszerzenia. | Struts zmuszają kontrolery do rozszerzania klas Struts
| Spring MVC posiada interfejs HandlerMapping do obsługi tej funkcji. | W Struts, `Akcje` są dołączone do widoku poprzez zdefiniowanie `ActionForwards` w `ActionMapping` lub globalnie.
| W `Spring MVC`, zatwierdzające są obiekty biznesowe, które nie są zależne od `Servlet API`, co sprawia, że można je ponownie wykorzystać w logice biznesowej przez obiekt bazodanowy. | W Struts walidacja jest zazwyczaj wykonywane (realizowana) w metodzie validate w `ActionForm`.

## Kontroler w Spring MVC framework
- Kontrolery zapewniają dostęp do zachowania aplikacji, które zazwyczaj definiowana poprzez interfejs usługi.
- Kontrolery interpretuje dane wprowadzone przez użytkownika i przekształcają je w model, który jest przedstawiony użytkownikowi przez widok.
- Spring realizuje kontroler w bardzo abstrakcyjny sposób, który pozwala tworzyć szeroką gamę kontrolerów.

## Adnotację `@Controller`.
- Adnotacja `@Controller` wskazuje, że dana klasa służy w aplikacji jako kontroler.
- Spring nie wymaga żadnego rozszerzenie klasy bazowej kontrolera lub odwołań do Servlet API.

## Użycie adnotacji `@RequestMapping`.
Adnotacja `@RequestMapping` jest używana do mapowania URL do całej klasy lub wybranej metody przechwytującej.

## Technologie Widoków
- `Freemarker`
- `JSP`
- `Tiles`
- `Velocity`
- `XLST` itp.
- można też stworzyć swój własny mechanizm niestandardowych widoków realizując interfejs `View`.