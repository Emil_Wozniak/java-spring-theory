# Hibernate

[[_TOC_]]

## Dostęp do Hibernate w Spring
Istnieją dwa sposoby uzyskania dostępu do Hibernate za pomocą Spring:

- Odwrócenie sterowania z `HibernateTemplate` i `Callback`.
- Rozszerzanie `HibernateDAOSupport` i stosowanie węzła `AOPInterceptor`.

## Wsparcie ORM
Spring wspiera następujące ORM:

- JPA (Java Persistence API)
- Hibernate
- TopLink
- JDO (Java Data Objects)
- OJB
- iBatis

## Efektywne stosowanie JDBC
JDBC mogą być stosowany bardziej efektywnie przy pomocy szablonu klas udostępnianych przez framework zwanych `JdbcTemplate`.

## Używanie JdbcTemplate
- Dzięki wykorzystaniu platformy `Spring JDBC` ciężar zarządzania zasobami i obsługi błędów jest zredukowany do minimum. 
- Pozostawia programistom pisać segmenty i zapytania, aby wyciągnąć dane z bazy danych. 
- `JdbcTemplate` zapewnia wiele metod wygodnych do wykonywania czynności takich jak konwersja danych z prymitywnych 
typów bazo danych lub obiektów, wykonywanie zapytań i zapewnienia standardowej obsługi błędów bazy danych.

## Typy zarządzania transakcjami
Spring obsługuje dwa typy zarządzania transakcjami:

- **Programmatic transaction management** (programowe): 
    - Oznacza to, że zarządzanie transakcjami jest wmieszane w kodzie biznesowym aplikacji.
    - To daje maksymalną elastyczność, ale jest trudna do utrzymania.
- **Declarative transaction management** (Deklaratywne): 
    - Oznacza to, oddzielenie zarządzania transakcjami od kodu biznesowego. 
    - Używamy tylko adnotacji lub bazowej konfiguracji opartej o XML do zarządzania transakcjami.

## Preferowany typ zarządzania transakcjami
- Deklaratywne zarządzanie transakcjami jest bardziej preferowane niż programowe zarządzania transakcjami.
- mniej elastyczne niż programowe, które pozwala na kontrolowanie transakcji bezpośrednio w kodzie.
