# SOLID

[[_TOC_]]

## Czym jest SOLID.
- jest akronimem 5 zasad OOP.
- zasady pozwalają wprowadzić łatwiejszy w utrzymaniu, czytelniejszy, zrozumiały i elastyczny kod.

## Single Responsibility
*klasa powinna posiadać tylko jeden powód istnienia i jeden powód do zmian*
- jak pomaga:
    - mniej kodu do testowania, mniej przypadków testów
    - mniej funkcjonalności mniej zależności
- mniejsze klasy łatwiej zorganizować w projekcie
- lepiej jest mieć wiele małych klas, metod i komponentów niż jeden duży.

## Open/Closed
*element powinien być otwarty na rozbudowę, ale zamknięty na modyfikację*.
- realizujemy poprzez abstrakcję (klasa abst. lub interface).
- jest łamana, kiedy kod trzeba **otworzyć** czyli zmienić jego zawartość w dowolny sposób.
- można ją zachować poprzez stworzenie interface'u, którego implementacje realizują to samo zadanie w różny sposób.

## Liskov Substitution
*typ bazowy powinien mieć możliwość podstawienia pochodnych w jego miejsce*.
- nie powinna istnieć różnica w wadze w podstawianiu typów pochodnych.
- Design by Contract:
    - Warunek wstępny: typ pochodny nie może być mocniejszy niż bazowy.
    - Warunek końcowy: typ pochodny nie może być słabszy niż bazowy.
    - niezmiennik klasy: warunek, jaki zawsze musi być spełniony.

## Interface Segregation
*Klasa nie powinna być zależna, od których metod nie używa*.
- lepiej jest mieć kilka interface'ów dla różnych funkcjonalności.

## Dependency Inversion
*Moduły wysokiego poziomu nie powinny zależeć od modułów niskiego poziomu, powinny zależeć od abstrakcji*.
*Abstrakcje nie powinny zależeć od detali. To detale powinny zależeć od abstrakcji*.
- nie jest spełniona, gdy klasa tworzy inną i jest zależna od niej w pełni.

