# Transakcyjność

[MIMUW](http://mst.mimuw.edu.pl/lecture.php?lecture=bad&part=Ch7)

## Transakcje
- pojęcie dotyczące baz danych
- dostarczają mechanizmów synchroniczności
- istotą transakcji jest integracja kilku operacji w niepodzielną całość

## Transakcja
- **To ciąg operacji do wspólnego niepodzielnego wykonania**
- Wymaga zachowania ACID

### ACID 
- Atomicity: *Niepodzielność*
    - wszystko albo nic
    - transakcja nie może być wykonana częściowo
- Consistency: *Integralność*
    - muszą zostać spełnione wszystkie warunki poprawności nałożone na bazę danych 
- Isolation: *Izolacja*
    - efekt równoległego wykonywania kilku operacji musi być szeregowalny
- Durability: *Trwałość*
    - po udanym zakonczeniu transakcji jej efekt zostaje na stałe w bazie danych

## Wycofanie tranzakcji
- może zostać wycofana w dowolnym momencie
- wszystkie zmiany zostają zignorowane

## Transakcje w SQL

`BEGIN [WORK];` - otwarcie

`COMMIT;` - zamknięcie lub anulowanie

`ROLLBACK;` - przy wystąpieniu błędu, np: naruszenia ograniczeń. Następuje niejawne wycofanie zmian

`SET TRANSACTION LEVEL READ ONLY;` - ustawia operacje wyłącznie na czytanie

`SET TRANSACTION LEVEL READ WRITE;` - domyśly poziom tranzakcji

`SET TRANSACTION ISOLATION LEVEL [READ COMMITTED | SERIALIZABLE];` - poziomy izolacji

- `SERIALIZABLE` - gwarantuje semantykę sekwencyjną \(**ACID**\)
- `READ COMMITTED` - powoduje przy modyfikacjach czekanie na zwolnienie blokad wierszy, odczyt nie jest powtarzalny.
- inne standardy:
    - `REPEATABLE READ` - gdy odczyty w ramach tranzakcji dają te same wiersze co poprzednio mogą dawać 'fantomy'
    - `READ UNCOMMITED` - pozwala na brudny odczyt, czytanie danych jeszcze nie zatwierdzonych





