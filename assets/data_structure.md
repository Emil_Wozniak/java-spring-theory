# Struktury Danych w Java

[[_TOC_]]

## Listy teoria

- są podobne do tablic.
- listy wiązane
    - jednokierunkowa
    - dwukierunkowa
    - cykliczna
    - acykliczna
    
### Acykliczna jednokierunkowa lista
- ostatni element jest nullem (głową listy).
- elementy poprzedzające głowę to ogon
- nie posiada referencji do poprzedzającego elementu.

```mermaid
graph LR;
    A-->B;
    B-->C;
    C-->null;
```

### Dwukierunkowe listy
- kazdy node przechowuje dodatkowo referencję node'u poprzedzającego go.
```mermaid
graph LR;
    A-->B;
    B-->C;
    C-->null;
    B-->A;
    C-->B;
```

### Cykliczne listy
- głowa zamiast null przechowuje referencję do 1 elementu.

#### Lista dwukierunkowa cykliczna

```mermaid
graph LR;
    A-->B;
    B-->C;
    C-->D;
    B-->A;
    C-->B;
    D-->A;
```

## Metody
- Lista powinna posiadać zestaw typowych metod:
```java
/**
 * T - List elements type
 */
public interface List<T> {
    add(T obj); // dodaje element określonego typu
    delete(); // usuwa element
    get(int index); // pobiera element wg indeksu
    set(T obj, int index); // ustawia element w określonym indeksie
    size(); // zwraca rozmiar listy
    isEmpty(); // zwraca true jeśli lista jest pusta
    clear(); // usuwa wszystkie elementy listy
}
```
- lista może posiadać dodatkowe metody użytkowe.
    - getFirst()
    - getLast()
    - removeFirst()

## Framework kolekcji
- korzyści
    - zmniejsza wysiłek rozwojowy.
    - zwiększa jakość.
    - dostarcza dobrej dokumentacji.


```mermaid
graph TD;
    Collections-->Collection;
    Collections-->Map;
    Collection-->Iterable;
    List-->Collection;
    Queue-->Collection;
    Set-->Collection;
    ArrayList-->List;
    LinkedList-->List;
    Vector-->List;
    Stack-->Vector;
    Dequeue-->Queue;
    SortedSet-->Set;
    HashSet-->Set;
    LinkedHashSet-->Set;
    TreeSet-->SortedSet;
    TreeMap-->SortedMap;
    SortedMap-->Map;
    LinkedHashMap-->HashMap;
    HashMap-->Map;
```

### Lista (List)
- uporządkowana zbiór obiektów, których elementy mogą się powtarzać.
- elementy można w dowolnym miejscu:
    - dodawać
    - usuwać
    - zmieniać 
### Kolejka (Queue)
- Kolejka jest również uporządkowana, ale posiadająca dostęp jedynie do ostatniego elementu. 
- porządek działania FIFO (first-in-first-out) 

### Zestaw (Set)
- Nie jest uporządkowany.
- Nie może posiadać duplikatów.

### Mapa (Map)
- mapuje po kluczach i wartościach, nie dziedziczy po interface'ie Collection.
- każdy klucz musi odpowiadać przynajmniej wartości. 

## Collection i Collections
- Collection jest interface'em.
- Collections jest klasą użytkową.



