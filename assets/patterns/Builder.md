# Budowniczy

[[_TOC_]]

## Zastosowanie
- daje możliwość tworzenia złożonych obiektów etapami, krok po kroku.
- pozwala produkować różne typy oraz reprezentacje obiektu używając tego samego kodu konstrukcyjnego.

Wzorzec projektowy Budowniczy proponuje ekstrakcję kodu konstrukcyjnego obiektu z jego klasy i umieszczenie go w 
osobnych obiektach zwanych budowniczymi.

```mermaid 
classDiagram
Director --> Builder
Builder <.. CarBuilder
Builder <.. CarManualBuilder
CarBuilder --> Car
CarManualBuilder --> Manual
Builder *-- CarType
Builder *-- Engine
Builder *-- GPSNavigator
Builder *-- Transmission
Builder *-- TripComputer
Client --> Director
Client ..> CarBuilder
Client ..> CarManualBuilder

class Builder{
    <<interface>>
    setCarType(CarType type);
    setSeats(int seats);
    setEngine(Engine engine);
    setTransmission(Transmission transmission);
    setTripComputer(TripComputer tripComputer);
    setGPSNavigator(GPSNavigator gpsNavigator);
}
class CarBuilder{
    - CarType type;
    - int seats;
    - Engine engine;
    - Transmission transmission;
    - TripComputer tripComputer;
    - GPSNavigator gpsNavigator;

    + setCarType(CarType type)
}
class CarManualBuilder{
    - CarType type;
    - int seats;
    - Engine engine;
    - Transmission transmission;
    - TripComputer tripComputer;
    - GPSNavigator gpsNavigator;

    + setCarType(CarType type)
}
class Car {
    - CarType type;
    - int seats;
    - Engine engine;
    - Transmission transmission;
    - TripComputer tripComputer;
    - GPSNavigator gpsNavigator;
    - double fuel = 0
}
class Manual {
    - CarType type;
    - int seats;
    - Engine engine;
    - Transmission transmission;
    - TripComputer tripComputer;
    - GPSNavigator gpsNavigator;
    - final double fuel = 0
}
class CarType {
    <<Enumaration>>
    CITY_CAR, SPORTS_CAR, SUV
}
class Engine {
    - final double volume;
    - double mileage;
    - boolean started;
    on()
    off()
    isStarted()
    go(double mileage)
    getVolume()
    getMileage()
}
class GPSNavigator {
    - String route;
    getRoute()
}

class Transmission {
    <<Enumaration>>
    SINGLE_SPEED, MANUAL, AUTOMATIC, SEMI_AUTOMATIC
}

class TripComputer {
    - Car car
    showFuelLevel()
    showStatus()
}

class Director {
    constructSportsCar(Builder builder)
    constructCityCar(Builder builder)
    constructSUV(Builder builder)
}
```

## Możliwość zastosowania

###### *Stosuj wzorzec Budowniczy, aby pozbyć się „teleskopowych konstruktorów”.*
- Załóżmy, że masz konstruktor, przyjmujący 10 opcjonalnych parametrów. Wywołanie takiego potwora jest co najmniej 
niewygodne, dlatego przeciążamy konstruktor i tworzymy wiele jego krótszych wersji, wymagających mniej parametrów. 
Będą one wciąż odwoływać się do głównego konstruktora, przekazując jakieś domyślne wartości w miejsce pominiętych 
argumentów.
- Wzorzec Budowniczy pozwala konstruować obiekty krok po kroku, w miarę jak staje się to w programie potrzebne. 
Po zaimplementowaniu tego wzorca nie musisz przekazywać konstruktorowi tuzina parametrów.

###### *Stosuj wzorzec Budowniczy, gdy potrzebujesz możliwości tworzenia różnych reprezentacji jakiegoś produktu (na przykład, domy z kamienia i domy z drewna).*
- Wzorzec Budowniczy można użyć, gdy konstruowanie różnorakich reprezentacji produktu obejmuje podobne etapy, które 
różnią się jedynie szczegółami.
- Bazowy interfejs budowniczego definiuje wszelkie możliwe etapy konstrukcji, a konkretni budowniczy implementują te 
kroki, by móc tworzyć poszczególne reprezentacje obiektów. Natomiast klasa kierownik pilnuje właściwego porządku 
konstruowania.

###### *Stosuj ten wzorzec do konstruowania drzew Kompozytowych lub innych złożonych obiektów.*

- Wzorzec budowniczego umożliwia konstrukcję w etapach. Niektóre z nich możemy odroczyć bez szkody dla finalnego 
produktu. Możemy nawet wywoływać etapy rekursywnie, co przydaje się przy budowie drzewa obiektów.
- Budowniczy uniemożliwia dostęp do nieskończonego produktu przez okres jego konstrukcji. Zapobiega to pozyskiwaniu 
niekompletnych wyników przez kod kliencki.

