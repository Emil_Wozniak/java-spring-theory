# Adapter

[[_TOC_]]

## Zastosowanie

Pozwala na współdziałanie ze sobą obiektów o niekompatybilnych interfejsach.

```mermaid 
classDiagram
Client --|> ClientInterface
ClientInterface <.. Adapter
Adapter --|> Service
class Client {
    business()
}
class ClientInterface{
    <<interface>>
    logic()
}
class Adapter{
    -adaptee: Service
    logic() 
}
class Service{
    serve()
}
```

## Możliwość zastosowania

###### *Stosuj klasę Adapter, gdy chcesz wykorzystać jakąś istniejącą klasę, ale jej interfejs nie jest kompatybilny z resztą twojego programu.*
- Wzorzec Adapter pozwala utworzyć klasę, która stanowi warstwę pośredniczącą pomiędzy twoim kodem a klasą pochodzącą
 z zewnątrz lub inną, posiadającą jakiś nietypowy interfejs.

###### *Stosuj ten wzorzec, gdy chcesz wykorzystać ponownie wiele istniejących podklas, którym brakuje jakiejś wspólnej funkcjonalności, niedającej się dodać do ich nadklasy.*

- Możesz rozszerzyć każdą podklasę i umieścić potrzebną funkcjonalność w nowych klasach pochodnych. 
Jednak wtedy trzeba by było duplikować kod i umieścić go we wszystkich nowych klasach, a to psuje zapach kodu.
  
- Znacznie bardziej eleganckim rozwiązaniem jest umieszczenie brakującej funkcjonalności w klasie adapter i opakowanie
nią obiektów pozbawionych potrzebnych funkcji. Aby to zadziałało, klasy docelowe muszą mieć wspólny interfejs, 
a pole adaptera musi być z nim zgodne. Podejście to bardzo przypomina wzorzec Dekorator.

## Props / Cons
|Props :heavy_check_mark:                                                                                                                                                                |Cons :x:                                                                                                                                                                                          |
|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Zasada pojedynczej odpowiedzialności. Można oddzielić interfejs lub kod konwertujący dane od głównej logiki biznesowej programu.                                                       | Ogólna złożoność kodu zwiększa się, ponieważ trzeba wprowadzić zestaw nowych interfejsów i klas. Czasem łatwiej zmienić klasę udostępniającą jakąś potrzebną usługę, aby pasowała do reszty kodu.
| Zasada otwarte/zamknięte. Można wprowadzać do programu nowe typy adapterów bez psucia istniejącego kodu klienckiego, o ile będzie on korzystał z adapterów poprzez interfejs kliencki. |

