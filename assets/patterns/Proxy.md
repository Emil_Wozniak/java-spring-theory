# Proxy

[[_TOC_]]

## Zastosowanie
- pozwala stworzyć obiekt zastępczy w miejsce innego obiektu.
- nadzoruje dostęp do pierwotnego obiektu, pozwalając na wykonanie jakiejś czynności przed lub po przekazaniu do niego żądania.

Zakłada stworzenie nowej klasy pośredniczącej, o takim samym interfejsie co pierwotny obiekt udostępniający usługę. 

```mermaid 
classDiagram
Client --|> YouTubeDownloader : 
ThirdPartyYouTubeClass ..> ThirdPartyYouTubeLib : 
YouTubeCacheProxy ..> ThirdPartyYouTubeLib : 
YouTubeCacheProxy *-- Video : 
YouTubeDownloader o-- ThirdPartyYouTubeLib : 

class ThirdPartyYouTubeLib{
    <<interface>>
    HashMap<String, Video> popularVideos();
    getVideo(String videoId);
}
class ThirdPartyYouTubeClass{
    random(int min, int max)
    experienceNetworkLatency()
    connectToServer(String server)
    getRandomVideos()
}
class Video {
      + String id;
      + String title;
      + String data;
}

class YouTubeCacheProxy {
    - ThirdPartyYouTubeLib youtubeService;
    - HashMap<String, Video> cachePopular;
    - HashMap<String, Video> cacheAll;
    reset()
}
class YouTubeDownloader{
    ThirdPartyYouTubeLib api;
    renderVideoPage(String videoId)
    renderPopularVideos()
}
class Client{
      YouTubeDownloader naiveDownloader
      YouTubeDownloader smartDownloader
}
```

## Możliwość zastosowania

Wzorzec Pełnomocnik może przydać się w wielu przypadkach. Przejrzyjmy najpopularniejsze przypadki użycia tej koncepcji.

###### *Leniwa inicjalizacja (wirtualny pełnomocnik). Gdy masz do czynienia z zasobożernym obiektem usługi, którego potrzebujesz jedynie co jakiś czas.*
- Zamiast tworzyć obiekt podczas uruchamiania aplikacji, możesz opóźnić inicjalizację obiektu do momentu, gdy faktycznie staje się potrzebny.

###### *Kontrola dostępu (pełnomocnik ochronny). Przydatne, gdy chcesz pozwolić tylko niektórym klientom na korzystanie z obiektu usługi. Na przykład, gdy usługi stanowią kluczową część systemu operacyjnego, a klienci to różne uruchamiane aplikacje (również te szkodliwe).*
- Pełnomocnik może przekazać żądanie obiektowi usługi tylko wtedy, gdy klient przedstawi odpowiednie poświadczenia.

###### *Lokalne uruchamianie zdalnej usługi (pełnomocnik zdalny). Użyteczne, gdy obiekt udostępniający usługę znajduje się na zdalnym serwerze.*

-  W takim przypadku pełnomocnik przekazuje żądania klienta przez sieć, biorąc na siebie kłopotliwe szczegóły przesyłu.

