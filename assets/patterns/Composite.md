# Composite

## Zastosowanie

Pozwala skomponować obiekty w strukturach drzewiastych i pracować z tymi strukturami jakby były one indywidualnymi 
obiektami.

Zastosowanie wzorca Composite ma sens jedynie, jeśli podstawowy model aplikacji można zaprezentować w postaci 
drzewiastej.

Dla przykładu mamy dwa typy obiektów Products i Boxes. Box może zawierać kilka obiektów typu Product, 
jak również kilka mniejszych obiektów typu Box. Te mniejsze obiekty typu Box również mogą zawierać jakieś obiekty 
typów Box lub Product, i tak dalej.

Załóżmy, że zdecydujemy obliczyć wartość tych produktów i pudełek. Aby to zrobić, musielibyśmy znać dokładnie 
strukturę tych pudełek i produktów w trakcie pisania program.

```mermaid 
classDiagram
ImageEditor --* EditorCanvas : Composition
ImageEditor --o CompoundShape : Aggregation
Shape<..BaseShape : Dependency
BaseShape <|-- Rectangle : Inheritance
BaseShape <|-- Dot : Inheritance
Shape --* Color : Aggregation
CompoundShape --|> BaseShape
CompoundShape --o BaseShape
class Shape{
    <<interface>>
    Color color
    draw()
    getX();
    getY();
    getWidth();
    getHeight();
}
class BaseShape{
  <<abstract>>
    Color color
    draw()
    getX();
    getY();
    getWidth();
    getHeight();
}
class Color{
    <<enumeration>>
    RED
    BLUE
    GREEN
    WHITE
    BLACK
}
class Dot {
    int DOT_SIZE = 3
    Color color
    draw()
}
class Rectangle {
    int width
    int height
    Color color
    draw()
}
class CompoundShape{
    List<Shape> children
    add(Shape component)
    add(Shape... components)
    remove(Shape child)
    remove(Shape... components)
    clear()
}
class ImageEditor {
    - EditorCanvas canvas
    - CompoundShape allShapes
}
```

## Możliwość zastosowania

###### *Użyj wzorca złożonego, gdy musisz zaimplementować drzewiastą strukturę obiektu.*
- Wzorzec Composite zapewnia dwa podstawowe typy elementów, które mają wspólny interfejs: proste liście 
i złożone pojemniki. Pojemnik może składać się zarówno z liści, jak i innych pojemników. Umożliwia to zbudowanie 
zagnieżdżonej struktury obiektów rekurencyjnych, która przypomina drzewo.


###### *Użyj wzorca, jeśli chcesz, aby kod klienta traktował jednolicie zarówno proste, jak i złożone elementy.*

- Wszystkie elementy zdefiniowane przez wzór złożony mają wspólny interfejs. Korzystając z tego interfejsu, klient 
nie musi się martwić o konkretną klasę obiektów, z którymi współpracuje.

## Props / Cons
|Props :heavy_check_mark:                                                                                                                            |Cons :x:                                                                                                                                                                                                                 |
|----------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Możesz pracować ze złożonymi strukturami drzew wygodniej: użyj polimorfizmu i rekurencji na swoją korzyść.                                         | Zapewnienie wspólnego interfejsu dla klas, których funkcjonalność zbyt się różni, może być trudne. W niektórych scenariuszach konieczna jest nadmierna generalizacja interfejsu komponentu, co utrudnia jego zrozumienie.
| Zasada Open / Close . Możesz wprowadzić nowe typy elementów do aplikacji bez przerywania istniejącego kodu, który teraz działa z drzewem obiektów. |

