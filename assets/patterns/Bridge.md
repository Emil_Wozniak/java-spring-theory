# Bridge pattern

## Zastosowanie

- oddzielenie 1 dużej klasy na zestaw blisko związanych ze sobą klas
 w 2 oddzielnych hierarchiach, które mogą być rozwijane osobno.
- abstrakcja i implementacja

Problem:
```mermaid 
classDiagram
    Shape<|--RedCircle
    Shape<|--BlueCircle
    Shape<|--RedSquare
    Shape<|--BlueSquare
    Shape: +Props props
```

- Problem rozwiązać problem przechodzimy z *dziedziczenia* do **kompozycji** obiektu. 
- Do oryginalnego obiektu dodajemy referencję do osobnej hierarchii klas.

Problem:
```mermaid 
classDiagram
Shape<|--Circle
Shape<|--Square
Shape*--Color
class Shape{
    <<interface>>
    Color color
    draw()
}
class Color{
    <<enumeration>>
    RED
    BLUE
    GREEN
    WHITE
    BLACK
}
class Square {
    Color color
    draw()
}
class Circle {
    Color color
    draw()
}
```

### Abstrakcja i implementacja

Abstrakcja (zwana również interface’em) jest warstwą wysokiego poziomu kontroli encji.
 Ten poziom nie ma za zadanie wykonywać żadnej realnej pracy, powinien przekazywać 
 tą pracę do warstwy implementacji (zwanej platformą).
 
 ## Struktura
1. **Abstraction** wprowadza kontrolę logiki wysokiego poziomu. 
Opiera się na implementacji obiekcie, aby wykonać aktualną pracę niskiego poziomu.
 
2. **Implementation** deklaruję wspólny dla wszystkich konkretnych implementacji interface. 
Abstrakcja może się komunikować z implementacją obiektu za pomocą metod, które są zadeklarowane tutaj. 
Abstrakcja może listować te same metody co implementacja, ale zazwyczaj abstrakcja deklaruje bardziej złożone 
zachowanie, które opierają się na szerszej gamie prymitywnych operacji zadeklarowanych przez implementację.
 
3. **Concrete Implementations** zawiera kod specyficzny dla platformy.
 
4. **Refined Abstractions** wprowadza warianty kontroli logiki. Tak jak jej rodzic, pracują z różnymi implementacjami 
 za pomocą ogólnej implementacji interface’u.
 
 ## Możliwość zastosowania
 
 ###### *Użyj wzorca mostu, jeśli chcesz podzielić i zorganizować klasę monolityczną, która ma kilka wariantów niektórych funkcji (na przykład, jeśli klasa może współpracować z różnymi serwerami bazy danych).*
 - Im większa staje się klasa, tym trudniej jest dowiedzieć się, jak to działa, i tym dłużej trwa zmiana. Zmiany wprowadzone w jednym z wariantów funkcjonalności mogą wymagać wprowadzenia zmian w całej klasie, co często powoduje błędy lub nie zajmuje się niektórymi krytycznymi skutkami ubocznymi.
 
 - Wzorzec mostu pozwala podzielić klasę monolityczną na kilka hierarchii klas. Następnie możesz zmienić klasy w każdej hierarchii niezależnie od klas w innych. Takie podejście upraszcza obsługę kodu i minimalizuje ryzyko uszkodzenia istniejącego kodu.
 
 ###### *Użyj wzorca, gdy chcesz rozszerzyć klasę na kilka ortogonalnych (niezależnych) wymiarów.*
 
 - Most sugeruje wydzielenie osobnej hierarchii klas dla każdego z wymiarów. Oryginalna klasa deleguje powiązaną pracę do obiektów należących do tych hierarchii, zamiast robić wszystko samodzielnie.
 
 ###### *Użyj mostu, jeśli chcesz mieć możliwość przełączania implementacji w czasie wykonywania.*
 - Chociaż jest to opcjonalne, wzorzec mostu pozwala zastąpić obiekt implementacji wewnątrz abstrakcji. To tak proste, jak przypisanie nowej wartości do pola.
 - Nawiasem mówiąc, ten ostatni przedmiot jest głównym powodem, dla którego tak wiele osób myli Most z modelem Strategii. Pamiętaj, że wzorzec jest czymś więcej niż tylko pewnym sposobem na uporządkowanie twoich klas. Może również komunikować zamiary i rozwiązywać problemy.
