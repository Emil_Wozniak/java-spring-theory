# Spring Framework

[[_TOC_]]

## Spring
- Framework open source dedykowany dla Java enterprise
- Programowanie w J2EE było łatwiejsze oraz promowanie dobrych praktyk.
- Bazuje na modelu programowania na POJO.

## Działanie
- przed Spring **Beany** były zarządzane w *JEE* bezpośrednio przez klasy z nich korzystające,
 tworząc silne powiązania.
- Spring wprowadził *Kontener IoC* dla aplikacji Java.
- *Kontener IoC* zajmuje się tworzeniem i zarządzaniem **Beanami**.
- *IoC* to akronim od **Inversion of Control**
- **Dependency Injection** jest jednym ze sposobów realizacji wzorca *IoC*.

## Korzyści
- **Lekkość**: wielkość i przejrzystość.
- **IoC**: luźne powiązania między klasami.
- **AOP**: oddzielenie logiki biznesowej od usług systemowych.
- **Kontener**: obiekty zawarte są i zarządzane oraz konfigurowane wewnątrz kontenera.
- **MVC**: Spring Web jest oparty o wzorzec MVC.
- **Transakcje**: spójny interface zarządzania transakcjami.
- **Wyjątki**: wygodne API do tłumaczenia wyjątków.

## Moduły
- **CORE**
- **Bean**
- **Context**
- **Expression Language**
- **JDBC**
- **ORM**
- **OXM**: Marshalling XML using O/X Mappers.
- **JMS**: Java Messaging Service.
- **Transaction**
- **Web**
- **Web-Servlet**
- **Web-Struts**
- **Web-Portlet**

## Konfiguracja
- Plik XML z informacją o klasach.
- Plik Java z Adnotacją `@Configuration`

## Dependency Injection
- Konwencja wg, której klasa nie tworzy swoich zależności, ale opisuje jak powinny być tworzone. 
- Polega na nie łączeniu ze sobą bezpośrednio komponentów w kodzie.
- Za łączenie komponentów odpowiada kontener IoC.

## Typy odwrócenia kontroli IoC
*Wstrzykiwania zależności*
- **Constructor-based DI**: realizowane, gdy kontener wywołuje konstruktor z wieloma argumentami, w których każdy 
reprezentuje zależności od innej klasy.
- **Setter-based DI**: przez fabrykę beanów

## Korzyści z używania IoC
- Minimalizacja ilości kodu
- Łatwość testowania
- Luźne powiązania
- wsparcie dla ładowania usług w trybach *eager* i *lazy* 

## AOP
Technika pozwalająca zmodularyzować związki lub zachowania projektowe, gdy przecinają typowe wymogi, 
np.: Logowanie, transakcje. 

## Typy kontenera IoC
- **BeanFactory**: 
    - najprostszy.
    - zapewnia podstawowe wsparcie dla DI.
    - jest wykorzystywany, gdy zasoby są ograniczone.
- **ApplicationContext**:
    - dodaje więcej funkcjonalności.
    - dodaje np.: zdolność czytania wiadomości tekstowych i wywoływania zdarzeń dla określonych beanów.

## BeanFactory
Najpowszechniej stosowanym przez BeanFactory jest implementacja klasy XmlBeanFactory. Pojemnik ten odczytuje metadane 
konfiguracji z pliku XML i używa go do tworzenia w pełni skonfigurowanego systemu lub aplikacji.

## ApplicationContext

Trzy powszechnie stosowane implementacje ApplicationContext są następujące:

- **FileSystemXmlApplicationContext**:
    - kontener ten ładuje definicje Beanów z pliku XML.
    - musi posiadać pełną ścieżkę dostępu do pliku konfiguracyjnego XML dla Beanów do konstruktora kontenera.
- **ClassPathXmlApplicationContext**: 
    - kontener ten ładuje definicje Beanów z pliku XML.
    - Tu nie trzeba podać pełną ścieżkę dostępu do pliku XML, ale trzeba ustawić `CLASSPATH` prawidłowo,
     ponieważ kontener będzie szukać plików XML z konfiguracją Beanów w `CLASSPATH`.
- **WebXmlApplicationContext**:
    - Pojemnik ten ładuje plik XML zawierający definicje wszystkich beanów z poziomu aplikacji internetowej.

## BeanFactory vs ApplicationContext
niektóre:
- **ApplicationContext** zapewniają środki do rozwiązywania wiadomości tekstowych, w tym wsparcie dla `i18n`.
- **ApplicationContext** posiadają podstawowe metody, do ładowania zasobów plików, takich jak obrazki.
- **ApplicationContext** umożliwia wysyłanie zdarzeń do beanów, które są zarejestrowane jako słuchające.
- **ApplicationContext** realizuje `MessageSource`, interfejs używany do uzyskania zlokalizowanych wiadomości w czasie
 rzeczywistym, gdy jest podłączany.

## Spring Bean

- Obiekty tworzące szkielet aplikacji.
- Są tworzone, montowane i zarządzane przez kontener **IoC**.
- Kontener je tworzy na podstawie deklaracji w pliku XML lub adnotacji.

## Konfiguracja metadanymi do kontenera
Istnieją następujące trzy ważne sposoby dostarczania konfiguracji metadanych do kontenera Spring:

- oparty o plik konfiguracyjny XML.
- konfiguracji opartej o Adnotacje.
- Konfiguracja Java.

## Zakresy Beanów
- **singleton**: Bean o jednej instancji.
- **prototype**: Bean może mieć dowolną liczbę instancji.
- **request**: ograniczony do żądania HTTP. Tylko w kontekście webowej wersji `ApplicationContext`.
- **session**: Bean ograniczony do zakresu sesji HTTP. Tylko w kontekście webowej wersji `ApplicationContext`.
- **global-session**: Bean ograniczony do zakresu globalnej sesji HTTP. Tylko w kontekście webowej wersji
 `ApplicationContext`

## Domyślny Zakres Beanów
Singleton

## Zakresie singleton a wykonanie wielowątkowe
Zakres Singleton nie jest zabezpieczony do wykonywania wielowątkowego.

## Cykle życia Bean
- **Instancjonowanie**: po pierwsze Spring kontener znajduje definicję Beana i instancjonuje Bean.
- **Wypełnianie właściwości**: używając wstrzykiwania zależności, Spring uzupełnia wszystkie właściwości
 określone w definicji Beana.
- **Ustawianie nazwy Beana**: Jeżeli Bean implementuje interfejs BeanNameAware, Spring ustawia id Beana 
za pomocą metody `setBeanName()`.
- **Ustawianie fabryki Beana**: Jeśli Bean implementuje interfejs BeanFactoryAware, Spring ustawia `BeanFactory`
 za pomocą metody `setBeanFactory()`.
- **Wstępna inicjalizacja**: zwany także *post-procesingiem* Beana. Jeśli są jakieś `BeanPostProcessors`
 związany z Beanem, Spring wywołuje metodę `postProcessBeforeInitialization()`.
- **Inicjalizacja Beana**: Jeżeli bean realizuje interfejs `InitializingBean`, jego metoda`afterPropertySet()`
 jest wywoływana. Gdy Bean posiada metodę inicjującą, to wywołuje ją.
- **Post inicjalizacja**: Jeśli jest jakiś `BeanPostProcessors` związany z Beanem, jego metoda 
`postProcessAfterInitialization()` zostanie wywołana.
- **Gotowy do użycia**: Teraz Bean jest gotowy do użycia przez aplikację.
- **Destroy**: Jeżeli Bean implementuje `DisposableBean` będzie wywołana metoda `destroy()`.

## `@Required`
- Właściwość Bean musi być wypełniona w konfiguracji.
- Jeśli nie zostanie spełniona wyrzuca `BeanInitializationException`.

## `@Qualifier`
- Przy tworzeniu wielu Beanów tego samego typu

## `@Component`
- klasa, która ma ustawioną powyższą adnotację, jest tworzona jako beana i ma nadany id takie jak nazwa klasy 
tylko małą literą.
- Taki bean może być wstrzykiwany adnotacyjnie czy też xml-owo do innych beanów.
- Specjalizacjami adnotacji `@Component` są:
    - `@Service`
    - `@Repository`
    - `@Controller`

## `@Service`
- Adnotacja używana jako stereotyp do definiowania warstwy usług.
- Dana klasa musi utworzyć beana i nadać mu id takie jak nazwa klasy tylko małą literą. 
- Taki bean może być wstrzykiwany adnotacyjnie czy też xml-owo do innych beanów.

## `@Repository`
- Adnotacja używana jako stereotyp do definiowania warstwy obiektów `persistans` Bean (używanych przy pracach z 
bazami danych). 
- Takie Beany są używane np. do tłumaczenia wątków z warstwy `persistans`. 
- Dana klasa musi utworzyć Beana i nadać mu id takie jak nazwa klasy tylko małą literą. 
- Taki bean może być wstrzykiwany adnotacyjnie czy też xml-owo do innych beanów.

## Różnica między `@Component` a `@Service`
- Poza tym, że `@Service` jest pochodną od `@Component` więcej różnic nie znajdziemy. 
- Działanie obu adnotacji jest takie same na kontener. 
- Wprowadzenie tych dwóch adnotacji było podyktowane tym by określić osobne stereotypy pozwalające na podział 
wielowarstwowy aplikacji. 
- W przypadku gdy prawidłowo używamy stereotypów, sprawa jest uproszczona i łatwo będzie nam zdefiniować
 odpowiedni `pointcut`.
 
## Adnotacje JSR-250
- **`@PostConstruct`**: Ta adnotacja może być stosowana jako alternatywna dla metody odpalanej po inicjalizacji.
- **`@PreDestroy`**: Ta adnotacja może być stosowana jako alternatywna metody wywoływanej po zniszczeniu obiektu.
- **`@Resource`**: Ta adnotacja może być stosowany do pól lub metod dostępowych. `@Resource` pobiera nazwę atrybutu
 i po tej nazwie znajduje Beana, który nazywa się tak samo i wstrzykuje go. Można powiedzieć, że łączenie zachodzi
  w trybie by-name.

## Przechwytywanie zdarzeń 
Przechwytywani zdarzeń w ApplicationContext zachodzi poprzez klasę `ApplicationEvent` i interfejs `ApplicationListener`. 
Jeśli Bean implementuje ApplicationListener, to za każdym razem, kiedy `ApplicationEvent` zostanie opublikowany w 
`ApplicationContext`, Bean jest zawsze o tym powiadamiany.

## Aspect
[AOP](./spring/Aspect.md)

## Hibernate
[Hibernate](./spring/Hibernate.md)

## MVC
[MVC](./spring/MVC.md)

## JDBC
[JDBC](./spring/JDBC.md)
