# Database

## Content
[[_TOC_]]

## Rodzaje baz danych
- **Proste**:
    - kartotekowe
    - hierarchiczne
- **Złożone**:
    - relacyjne
    - obiektowe
    - relacyjno-obiektowe
    - strumieniowe
    - temporalne
    - nierelacyjne

### kartotekowe
Nie może współpracować z innymi bazami.
Np.: książka telefoniczna, spis w książce.

### hierarchiczne
- baza powiązanych ze sobą danych.
- posiada pkt. początkowy i rozgałęzienia.
- przypomina strukturę danych, np.: folderów. 

### relacyjne
- W bazach relacyjnych wiele tablic danych może ze sobą współpracować.
- Posiadają wewnętrzne języki programowania.
- Wszystkie wartości są oparte na prostych typach danych.
- Wszelakie dane są reprezentowane w formie dwuwymiarowych tabel (wiersze i kolumny).
- Jest możliwość porównywania wartości z różnych kolumn.
- Musi w bazie znajdować się minimum jedna kolumna posiadająca niepowtarzalne wartości w granicach całej tabeli. 

### obiektowe
- dane są przechowywane za pomocą struktur obiektowych.
- przechowują obiekty o dowolnych strukturach wraz z przywiązanymi do nich metodami (procedurami).
- przewagę nad innymi rodzajami baz widać, kiedy zachodzi konieczność przechowywania bardzo złożonych struktur.
- wadą baz obiektowych jest problem z realizacją zapytań.

### relacyjno-obiektowe
- Bazy relacyjno-obiektowe pozwalają na manipulowanie danymi jako zestawem obiektów.
- posiadają jednak bazę relacyjną jako wewnętrzny mechanizm przechowywania danych.

### strumieniowe
- dane są przedstawione w postaci strumieni danych.
- System zarządzania taką bazą nazywany jest `DSMS` (ang. Data Stream Management System).

### temporalne
- Jest to odmiana bazy relacyjnej, w której każdy rekord posiada stempel czasowy.

### nierelacyjne
- nazywane NoSQL database.
- Pod pojęciem bazy nierelacyjnej najczęściej rozumie się przechowywanie danych w formie listy par obiektów klucz-wartość.
- W bazie NoSQL najczęściej nie ma wymagania, aby obiekty były jednorodne pod względem struktury.

## Najczęściej stosowane
Relacyjne


